var map;

map = new function () {

  //private vars
  var map;
  var marker;
  var myLatLng;
  var options;

  //catch DOM
  var $canvas = document.getElementById('map__canvas');

  //bind events
  $(window).bind('resize', function () {
    setTimeout(function () {
      map.setCenter(myLatLng);
    }, 0);
  });

  //public functions
  this.loadApi = function () {
    var script = document.createElement("script");
    script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBLlnLLh8HK7GRvGfX9bHeBk_Xa2G_1x50';
    document.body.appendChild(script);
  };

  this.init = function (mapLat, mapLng, mapZoom, mapMarker) {
    setOptions(mapLat, mapLng, mapZoom);
    setMap();
    setMarker(mapMarker);
  };

  //private functions
  var setOptions = function (latMap, lngMap, zoomMap) {
    myLatLng = new google.maps.LatLng(latMap, lngMap);

    options = {
      zoom: zoomMap,
      center: myLatLng,
      scrollwheel: false,
      panControl: false,
      zoomControl: true,
      streetViewControl: false,
      noClear: false,
      overviewMapControl: false,
      mapTypeControl: false,
      styles: []
    };
  };

  var setMap = function () {
    if ($canvas) {
      map = new google.maps.Map($canvas, options);
    }
  };

  var setMarker = function (mapMarker) {

    marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      icon: {
        url: mapMarker
      }
    });
  };
};

map.loadApi();