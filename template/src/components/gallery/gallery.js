var gallery;

gallery = new function () {

  //catch DOM
  var $el;
  var $slider;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  var init = function () {
    $el = $(".gallery");
    $slider = $el.find(".gallery__slider");

    $slider.lightGallery({
      mode: 'lg-slide'
    });

    $slider.slick({
      dots: false,
      arrows: false,
      slidesToShow: 4,
      slidesToScroll: 1,
      infinite: false,
      speed: 500,
      autoplay: true,
      autoplaySpeed: 1500,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        }
      ]
    });
  };
};