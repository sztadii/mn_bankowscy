var nav;

nav = new function () {

  //private functions
  var navBreakpoint = 1024;

  //catch DOM
  var $windowWidth;
  var $navButton;
  var $navBackground;
  var $navListMain;

  //bind events
  $(document).ready(function () {
    init();
  });

  $(window).resize(function () {
    $windowWidth = $(window).width();
  });

  //private functions
  var init = function () {
    catchVars();
    triggerNav();
  };

  var catchVars = function () {
    $windowWidth = $(window).width();
    $navButton = $('.nav__button');
    $navBackground = $('.nav .nav__background');
    $navListMain = $('.nav .nav__list');
  };

  var triggerNav = function () {
    $navButton.on('click', function () {
      toggleNav();
    });

    $navBackground.on('click', function () {
      toggleNav();
    });

    $('.nav__link').on('click', function () {
      toggleNav();
    });
  };

  var hideNav = function () {
    $navBackground.fadeOut(500);
    $navListMain.slideUp(400, function () {
      $navButton.removeClass('-active');
    });
  };

  var showNav = function () {
    $navBackground.fadeIn(500);
    $navListMain.slideDown(400, function () {
      $navButton.addClass('-active');
    });
  };

  var toggleNav = function () {
    if (navBreakpoint > $windowWidth) {
      if ($navButton.hasClass('-active')) {
        hideNav();
      } else {
        showNav();
      }
    }
  };
};