var news;

news = new function () {

  //catch DOM
  var $el;
  var $slider;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  var init = function () {
    $el = $(".news");
    $slider = $el.find('.news__slider');

    $el.imagesLoaded({background: true}).always(function () {
      $slider.slick({
        arrows: false,
        dots: true,
        dotsClass: 'slick__dots',
        customPaging: function () {
          return '<div class="slick__dot"></div>'
        },
        appendDots: $('.news'),
        slidesToShow: 4,
        slidesToScroll: 4,
        infinite: true,
        autoplay: true,
        vertical: true,
        cssEase: 'linear',
        draggable: false,
        responsive: [, {
          breakpoint: 600,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        }
        ]
      });
    });
  };
};